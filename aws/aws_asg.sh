#!/bin/bash

export AWS_DEFAULT_REGION=eu-central-1
export AWS_SECRET_ACCESS_KEY=`aws ssm get-parameters --names "access_key" --with-decryption --output text | awk '{print $4}'`
export AWS_ACCESS_KEY_ID=`aws ssm get-parameters --names "key_id" --with-decryption --output text | awk '{print $4}'`


echo "-----------------------------------"
echo "- Create AWS launch configuration -"
echo "-----------------------------------"

aws autoscaling create-launch-configuration \
        --launch-configuration-name `aws ssm get-parameters --names "LC_NAME" --with-decryption --output text | awk '{print $4}'` \
        --key-name dkhubulava-ec2 \
        --image-id ami-e28d098d \
        --security-groups sg-1fb57f75 \
        --instance-type t2.micro \
        --user-data file://DevOps028-infrastructure/aws/user_data.sh \
        --instance-monitoring Enabled=true \
        --iam-instance-profile ec2-admin-role


echo "----------------------------"
echo "- Create AWS load balancer -"
echo "----------------------------"
aws elb create-load-balancer \
        --load-balancer-name `aws ssm get-parameters --names "LB_NAME" --with-decryption --output text | awk '{print $4}'` \
        --listeners "Protocol=HTTP,LoadBalancerPort=80,InstanceProtocol=HTTP,InstancePort=9000" \
        --subnets subnet-90f53afb \
        --security-groups sg-1fb57f75


echo "-----------------------------------------"
echo "- Create AWS load balancer health check -"
echo "-----------------------------------------"
aws elb configure-health-check \
        --load-balancer-name `aws ssm get-parameters --names "LB_NAME" --with-decryption --output text | awk '{print $4}'` \
        --health-check Target=HTTP:9000/login,Interval=5,UnhealthyThreshold=5,HealthyThreshold=2,Timeout=2


echo "----------------------------------------------"
echo "- Add application url to AWS parameter store -"
echo "----------------------------------------------"
aws ssm put-parameter \
        --name "APP_URL" \
        --type "String" \
        --value "$(aws elb describe-load-balancers --load-balancer-names `aws ssm get-parameters --names "LB_NAME" --with-decryption --output text | awk '{print $4}'` | grep DNSName | awk '{print $2}' | cut -d'"' -f2)" \
        --overwrite



echo "--------------------------------"
echo "- Create AWS autoscaling group -"
echo "--------------------------------"

aws autoscaling create-auto-scaling-group \
    --auto-scaling-group-name `aws ssm get-parameters --names "ASG_NAME" --with-decryption --output text | awk '{print $4}'` \
    --launch-configuration-name `aws ssm get-parameters --names "LC_NAME" --with-decryption --output text | awk '{print $4}'` \
    --min-size 1 --max-size `aws ssm get-parameters --names "ASG_MAX_SIZE" --with-decryption --output text | awk '{print $4}'` \
    --desired-capacity 1 \
    --load-balancer-names `aws ssm get-parameters --names "LB_NAME" --with-decryption --output text | awk '{print $4}'` \
    --health-check-type ELB \
    --health-check-grace-period 300 \
    --availability-zones eu-central-1b

sleep 120
