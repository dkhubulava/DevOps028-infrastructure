#!/bin/bash

echo "-----------------------------"
echo "- Exporting AWS credentials -"
echo "-----------------------------"

export AWS_DEFAULT_REGION=eu-central-1
export AWS_SECRET_ACCESS_KEY=`aws ssm get-parameters --names "access_key" --with-decryption --output text | awk '{print $4}'`
export AWS_ACCESS_KEY_ID=`aws ssm get-parameters --names "key_id" --with-decryption --output text | awk '{print $4}'`


echo "-----------------------"
echo "- Create RDS instance -"
echo "-----------------------"

aws rds create-db-instance \
        --db-name `aws ssm get-parameters --names "DB_NAME" --with-decryption --output text | awk '{print $4}'` \
        --db-instance-identifier `aws ssm get-parameters --names "DB_INSTANCE_NAME" --with-decryption --output text | awk '{print $4}'` \
        --allocated-storage 5 \
        --db-instance-class db.t2.micro \
        --engine postgres \
        --master-username `aws ssm get-parameters --names "DB_USER" --with-decryption --output text | awk '{print $4}'` \
        --master-user-password `aws ssm get-parameters --names "DB_PASS" --with-decryption --output text | awk '{print $4}'` \
        --backup-retention-period 0 \
        --port `aws ssm get-parameters --names "DB_PORT" --with-decryption --output text | awk '{print $4}'` \
        --engine-version 9.6.2 \
        --storage-type gp2


echo "-------------------------"
echo "- Check database access -"
echo "-------------------------"

TARGET_STATUS=available
STATUS=unknown
while [[ "$STATUS" != "$TARGET_STATUS" ]]; do
        STATUS=`aws rds describe-db-instances \
                    --db-instance-identifier $(aws ssm get-parameters \
                                                --names "DB_INSTANCE_NAME" \
                                                --with-decryption \
                                                --output text | awk '{print $4}') | grep DBInstanceStatus | awk '{print$2}' | cut -d'"' -f2`
        
        echo "Database $INSTANCE : $STATUS ... "
        sleep 15
done


echo "----------------------------------------------------"
echo "- Add database host address to AWS parameter store -"
echo "----------------------------------------------------"

export DB_INSTANCE_INFO=`aws rds describe-db-instances \
                    --db-instance-identifier $(aws ssm get-parameters --names "DB_INSTANCE_NAME" --with-decryption --output text | awk '{print $4}') \
                    --query 'DBInstances[*].[DBInstanceIdentifier,Endpoint.Address,Endpoint.Port]' \
                    --output text`

aws ssm put-parameter --name "DB_HOST" --type "String" --value "$(echo $DB_INSTANCE_INFO | awk '{print $2}')" --overwrite
