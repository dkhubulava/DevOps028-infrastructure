#!/bin/bash

echo "----------------------------------"
echo "- OS update and install vim, git -"
echo "----------------------------------"
sudo yum update -y
sudo yum install vim git -y

echo "-----------------"
echo "- Install JDK 8 -"
echo "-----------------"
sudo yum remove java -y
sudo yum install java-1.8.0-openjdk -y


echo "--------------------------"
echo "- Export AWS credentials -"
echo "--------------------------"
export AWS_DEFAULT_REGION=eu-central-1
export AWS_SECRET_ACCESS_KEY=`aws ssm get-parameters --names "access_key" --with-decryption --output text | awk '{print $4}'`
export AWS_ACCESS_KEY_ID=`aws ssm get-parameters --names "key_id" --with-decryption --output text | awk '{print $4}'`


echo "----------------------"
echo "- Export DB variables-"
echo "----------------------"
export DB_NAME=`aws ssm get-parameters --names "DB_NAME" --with-decryption --output text | awk '{print $4}'`
export DB_USER=`aws ssm get-parameters --names "DB_USER" --with-decryption --output text | awk '{print $4}'`
export DB_PASS=`aws ssm get-parameters --names "DB_PASS" --with-decryption --output text | awk '{print $4}'`
export DB_HOST=`aws ssm get-parameters --names "DB_HOST" --with-decryption --output text | awk '{print $4}'`
export DB_PORT=`aws ssm get-parameters --names "DB_PORT" --with-decryption --output text | awk '{print $4}'`


echo "-------------------------------------------------"
echo "- Download liquibase and postgresql jdbc_driver -"
echo "-------------------------------------------------"
mkdir -p ~/samsara
cd ~/samsara

aws s3 cp s3://ssita-devops028-demo2/liquibase.tar.gz ~/samsara
tar xzf liquibase.tar.gz
cd liquibase

mkdir -p jdbc_driver
cd jdbc_driver
wget https://jdbc.postgresql.org/download/postgresql-42.1.4.jar

echo "-------------------------------------------"
echo "- Create liquibase.properties config file -"
echo "-------------------------------------------"
cd ..
cat <<EOF> liquibase.properties
driver: org.postgresql.Driver
url: jdbc:postgresql://$DB_HOST:$DB_PORT/$DB_NAME
username: $DB_USER
password: $DB_PASS
# specifies packages where entities are and database dialect, used for liquibase:diff command
referenceUrl=hibernate:spring:academy.softserve.aura.core.entity?dialect=org.hibernate.dialect.PostgreSQL9Dialect
EOF

echo "----------------------------------"
echo "- Download and execute liquibase -"
echo "----------------------------------"
mkdir -p bin
cd bin
wget https://github.com/liquibase/liquibase/releases/download/liquibase-parent-3.5.3/liquibase-3.5.3-bin.tar.gz
tar xzf liquibase-3.5.3-bin.tar.gz
./liquibase --classpath=../jdbc_driver/postgresql-42.1.4.jar --changeLogFile=../changelogs/changelog-main.xml --defaultsFile=../liquibase.properties update


echo "--------------------------------------------"
echo "- Download and run Samsara application -"
echo "--------------------------------------------"
aws s3 cp s3://ssita-devops028-demo2/Samsara-1.3.5.RELEASE.jar ~/samsara
cd ~/samsara 
java -jar Samsara-1.3.5.RELEASE.jar &
